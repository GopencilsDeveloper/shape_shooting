﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Editor Params
    [Header("UI Elements")]
    public GameObject menu;
    public GameObject inGame;
    public GameObject gameEnd;
    public GameObject gameOver;

    [Header("LEVEL")]
    public Text txtFromLV;
    public Text txtToLV;
    public Image On0;
    public Image On1;
    public Image On2;

    [Header("EFFECT")]
    public GameObject haloWin;
    public GameObject haloFail;
    public GameObject fireWork;
    #endregion

    #region Params
    private static UIManager _instance;

    #endregion

    #region Properties
    public static UIManager Instance { get => _instance; }
    #endregion

    #region Events
    #endregion

    #region Methods
    void Awake()
    {
        _instance = this;
    }

    public void StartMenu()
    {
        ShowMenu();
        HideInGame();
        HideGameEnd();
        HideGameOver();
    }
    public void InGame()
    {
        HideMenu();
        ShowInGame();
        HideGameEnd();
        HideGameOver();
    }
    public void GameEnd()
    {
        HideMenu();
        ShowInGame();
        ShowGameEnd();
        HideGameOver();
        ShowEffect(haloWin);
        ShowEffect(fireWork);
    }
    public void GameOver()
    {
        HideMenu();
        ShowInGame();
        HideGameEnd();
        ShowGameOver();
        ShowEffect(haloFail);
    }

    public void ShowMenu()
    {
        menu.SetActive(true);
    }
    public void ShowInGame()
    {
        inGame.SetActive(true);
    }
    public void HideMenu()
    {
        menu.SetActive(false);
    }
    public void HideInGame()
    {
        inGame.SetActive(false);
    }
    public void ShowGameEnd()
    {
        gameEnd.SetActive(true);
    }
    public void ShowGameOver()
    {
        gameOver.SetActive(true);
    }
    public void HideGameEnd()
    {
        gameEnd.SetActive(false);
    }
    public void HideGameOver()
    {
        gameOver.SetActive(false);
    }

    public void UpdateLevel()
    {
        int level = (int)(MapManager.Instance.currentMapOrder / 3) + 1;
        int map = (int)(MapManager.Instance.currentMapOrder % 3);
        print("Map: " + map);
        txtFromLV.text = level.ToString();
        txtToLV.text = (level + 1).ToString();
        switch (map)
        {
            case 0:
                On1.gameObject.SetActive(false);
                On2.gameObject.SetActive(false);
                break;
            case 1:
                On1.gameObject.SetActive(true);
                On2.gameObject.SetActive(false);
                break;
            case 2:
                On1.gameObject.SetActive(true);
                On2.gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void ShowEffect(GameObject halo)
    {
        StartCoroutine(C_ShowHaloEffect(halo));
    }

    IEnumerator C_ShowHaloEffect(GameObject halo)
    {
        halo.SetActive(true);
        yield return new WaitForSeconds(1f);
        halo.SetActive(false);
    }

    #endregion
}
