﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Theme
{
    public Color color0;
    public Color color1;
    public Color color2;
    public Color color3;
}

public class ThemeManager : MonoBehaviour
{
    #region Editor Params
    public List<Theme> themes;
    public LPWAsset.LowPolyWaterScript LPWS;
    #endregion

    #region Params
    private static ThemeManager instance;
    public Theme currentTheme;
    #endregion

    #region Properties
    public static ThemeManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods
    private void Awake()
    {
        instance = this;
    }
    public void RandomTheme()
    {
        currentTheme = themes[Random.Range(0, themes.Count)];
        RandomWaterColor(Random.ColorHSV(0f, 1f, 1f, 1f, 0.1f, 0.1f));
    }


    public void RandomWaterColor(Color color)   //Random.ColorHSV(0f, 1f, 1f, 1f, 0.1f, 0.1f) : Random H only
    {
        LPWS.material.SetColor("_Color", color);
    }
    #endregion
}
