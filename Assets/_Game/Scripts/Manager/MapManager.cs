﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    #region CONST
    public static string MapOrder = "MAPORDER";
    #endregion

    #region Editor Params
    public MapGenerator mapGenerator;
    public List<Texture2D> mapTextures;
    #endregion

    #region Params
    private static MapManager instance;
    private int index;
    public int currentTileEmptyAmount = -1;
    private bool isGameEnd = false;
    public int currentMapOrder;
    #endregion

    #region Properties
    public static MapManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods
    private void Awake()
    {
        instance = this;
        currentMapOrder = PlayerPrefs.GetInt(MapOrder);
    }
    
    private void Update()
    {
        if (currentTileEmptyAmount == 0)
        {
            GameManager.Instance.GameEnd();
            currentTileEmptyAmount = -1;
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.N))
        {
            index++;
            if (index >= mapTextures.Count)
            {
                index = 0;
            }
            currentMapOrder = index;
            // SaveMapOrder();
            mapGenerator.GenerateMap(index);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            index--;
            if (index < 0)
            {
                index = mapTextures.Count - 1;
            }
            currentMapOrder = index;
            // SaveMapOrder();
            mapGenerator.GenerateMap(index);
        }
#endif
    }

    public void GenerateMap()
    {
        mapGenerator.GenerateMap(currentMapOrder);
    }

    public void NextMap()
    {
        Debug.Log("NextMap");
        currentMapOrder++;
    }

    public void SaveMapOrder()
    {
        PlayerPrefs.SetInt(MapOrder, currentMapOrder);
        Debug.Log("Save Map Order");
    }

    #endregion
}
