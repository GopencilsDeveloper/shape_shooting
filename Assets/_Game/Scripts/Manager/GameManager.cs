﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, Menu, InGame, GameOver, GameEnd
}

public class GameManager : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    private static GameManager instance;
    public GameState currentState;
    #endregion

    #region Properties
    public static GameManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods
    void Awake()
    {
        Application.targetFrameRate = 60;
        instance = this;
    }

    private void Start()
    {
        StartMenu();
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
    }

    public void StartMenu()
    {
        Debug.Log("Menu!");
        ChangeState(GameState.Menu);
        UIManager.Instance.StartMenu();
        MapManager.Instance.GenerateMap();
    }

    public void PlayGame()
    {
        Debug.Log("Play Game!");
        ChangeState(GameState.InGame);
        UIManager.Instance.InGame();
        MapManager.Instance.GenerateMap();
        UIManager.Instance.UpdateLevel();
        ThemeManager.Instance.RandomTheme();
    }
    public void GameEnd()
    {
        Debug.Log("Game End!");
        ChangeState(GameState.GameEnd);
        MapManager.Instance.NextMap();
        MapManager.Instance.SaveMapOrder();
        UIManager.Instance.GameEnd();
        UIManager.Instance.UpdateLevel();

    }
    public void GameOver()
    {
        Debug.Log("Game Over!");
        ChangeState(GameState.GameOver);
        MapManager.Instance.SaveMapOrder();
        UIManager.Instance.GameOver();
        UIManager.Instance.UpdateLevel();

    }

    #endregion
}
