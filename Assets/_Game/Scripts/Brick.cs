﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    #region Editor Params
    public Vector3 direction;
    public float speed;
    public Rigidbody rgbd;
    public BoxCollider box;
    [HideInInspector]
    public bool isMoved;
    public MeshRenderer mesh;
    public GameObject popFade;
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        box.size = new Vector3(0.9f, 0.9f, 2f);
    }

    public void SetColor(Color col)
    {
        mesh.material.color = col;
    }

    private void Update()
    {
        if (isMoved)
        {
            transform.localPosition += direction * speed * Time.deltaTime;
        }
    }

    public void Move()
    {
        isMoved = true;
    }

    public void Stop()
    {
        isMoved = false;
        rgbd.isKinematic = true;
        box.size = new Vector3(1f, 1f, 2f);
        popFade.SetActive(true);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.CompareTag("Boundary"))
        {
            Stop();
            // transform.SetParent(other.transform.parent);
            transform.localPosition = RoundVector(transform.localPosition);
            transform.localScale = RoundVector(transform.localScale);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boundary"))
        {
            Destroy(gameObject);
        }
        if (other.CompareTag("TopRight"))
        {
            SetColor(ThemeManager.Instance.currentTheme.color0);
        }
        if (other.CompareTag("TopLeft"))
        {
            SetColor(ThemeManager.Instance.currentTheme.color1);
        }
        if (other.CompareTag("BotRight"))
        {
            SetColor(ThemeManager.Instance.currentTheme.color2);
        }
        if (other.CompareTag("BotLeft"))
        {
            SetColor(ThemeManager.Instance.currentTheme.color3);
        }
    }

    private Vector3 RoundVector(Vector3 v)
    {
        v.x = Mathf.Round(v.x);
        v.y = Mathf.Round(v.y);
        v.z = Mathf.Round(v.z);
        return v;
    }

    #endregion
}
