﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera cam;
    private void Start()
    {
        SetOrthoSize();
    }

    void SetOrthoSize()
    {
        float ratio = cam.aspect;
        if (ratio >= 0.75) // 3:4
        {
            Camera.main.fieldOfView = 9.5f;
        }
        else
        if (ratio >= 0.56) // 9:16
        {
            Camera.main.orthographicSize = 12.5f;
        }
        else
        if (ratio >= 0.45) // 9:19
        {
            Camera.main.orthographicSize = 14.5f;
        }
    }
}
