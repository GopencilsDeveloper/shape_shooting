﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    public int index;

    public List<Transform> wayPoints;

    private void Start()
    {
        transform.position = wayPoints[0].position;
    }

    private void Update()
    {
        if (!isMoving)
        {
            if (Input.GetKey(KeyCode.A))    //Left
            {
                Move(-1);
            }
            if (Input.GetKey(KeyCode.D))    //Right
            {
                Move(1);
            }
        }
    }

    private bool isMoving = false;

    public void Move(int direction)
    {
        StartCoroutine(C_Move(direction));
    }

    IEnumerator C_Move(int direction)
    {
        isMoving = true;
        index = direction < 0 ? (index - 1) : (index + 1);

        if (index == -1)
        {
            index = wayPoints.Count - 1;
        }
        if (index == wayPoints.Count)
        {
            index = 0;
        }

        Vector3 begin = transform.position;
        Vector3 end = wayPoints[index].position;

        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / 0.3f;
            transform.position = Vector3.Lerp(begin, end, t);
            yield return null;
        }
        isMoving = false;
    }
}
