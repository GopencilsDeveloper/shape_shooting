﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    #region CONST
    int[] cornersSquare = { 6, 18, 30, 42 };
    int[] cornersRectangle = { 6, 18, 30, 42 };
    #endregion

    #region Editor Params
    public LayerMask defaultLayer;
    public Collider boxCollider;
    public float stepDuration = 0.1f;
    public float shootingDelay = 0.15f;
    public Brick brickPrefab;
    public Transform mapParent;
    public Transform frontCheck;
    public List<Transform> wayPoints;
    #endregion

    #region Params
    private int index;
    // private int[] cornersIndex;
    private int direction;
    private bool isMoving = false;
    private bool isTouchMoved = false;
    private float distToUp;
    private int controlType = 0;
    private bool isShooting = false;
    private float firstTouchTime, touchDeltaTime;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Start()
    {
        // cornersIndex = cornersSquare;
        direction = Random.value < 0.5f ? -1 : 1;

        transform.position = wayPoints[0].position;  //Set position at 1st point.

        distToUp = boxCollider.bounds.extents.y;    //Get the distance to front.
    }

    public void SelectControll(int typeIndex = 0)
    {
        controlType = typeIndex;
    }

    private void Update()
    {
        if (GameManager.Instance.currentState == GameState.InGame)
        {
            if (controlType == 0)
            {
                AutoControl();
                stepDuration = 0.2f;
            }
            else
            {
                ManualControl();
                stepDuration = 0.075f;
            }
        }
    }

    void ManualControl()
    {
        if (!isMoving)
        {
#if UNITY_EDITOR
            if (Input.GetKey(KeyCode.A))    //Left
            {
                Move(-1);
            }
            if (Input.GetKey(KeyCode.D))    //Right
            {
                Move(1);
            }
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                if (!IsRightFront() && !IsCornered())
                {
                    ShootBrick();
                    if (!IsFront())
                    {
                        GameObject frontInstance = Instantiate(frontCheck.gameObject, frontCheck.position, Quaternion.identity, mapParent);
                        frontInstance.SetActive(true);
                    }
                }
            }
#elif UNITY_IPHONE || UNITY_ANDROID
            if (Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        isTouchMoved = false;
                    }
                    else
                    if (touch.phase == TouchPhase.Moved)
                    {
                        if (Mathf.Abs(touch.deltaPosition.x) >= 5f)
                        {
                            Move((int)Mathf.Sign(touch.deltaPosition.x));
                            isTouchMoved = true;
                        }
                    }
                    else
                    if (touch.phase == TouchPhase.Ended && !isTouchMoved)
                    {
                        if (!IsRightFront() && !IsCornered())
                        {
                            ShootBrick();
                            if (!IsFront())
                            {
                                GameObject frontInstance = Instantiate(frontCheck.gameObject, frontCheck.position, Quaternion.identity, mapParent);
                                frontInstance.SetActive(true);
                            }
                        }
                    }
                }
            }
#endif
        }
    }

    void AutoControl()
    {
        if (!isMoving)
        {
// #if UNITY_EDITOR
            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                if (!IsRightFront() && !IsCornered())
                {
                    ShootBrick();
                    if (!IsFront())
                    {
                        GameObject frontInstance = Instantiate(frontCheck.gameObject, frontCheck.position, Quaternion.identity, mapParent);
                        frontInstance.SetActive(true);
                    }
                }
                else
                {
                    Move(direction);
                }
            }
            else
            {
                Move(direction);
            }
// #elif UNITY_IPHONE || UNITY_ANDROID
//             if (Input.touchCount > 0)
//             {
//                 Touch touch = Input.GetTouch(0);
//                 if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
//                 {
//                     if (touch.phase == TouchPhase.Began)
//                     {
//                         firstTouchTime = Time.time;
//                         if (!IsRightFront() && !IsCornered())
//                         {
//                             ShootBrick();
//                             if (!IsFront())
//                             {
//                                 GameObject frontInstance = Instantiate(frontCheck.gameObject, frontCheck.position, Quaternion.identity, mapParent);
//                                 frontInstance.SetActive(true);
//                             }
//                         }
//                     }
//                     touchDeltaTime = Time.time - firstTouchTime;
//                     if (touchDeltaTime > 0.5f)
//                     {
//                         if (!IsRightFront() && !IsCornered())
//                         {
//                             ShootBrick();
//                             if (!IsFront())
//                             {
//                                 GameObject frontInstance = Instantiate(frontCheck.gameObject, frontCheck.position, Quaternion.identity, mapParent);
//                                 frontInstance.SetActive(true);
//                             }
//                         }
//                         else
//                         {
//                             Move(direction);
//                         }
//                     }
//                     if (touch.phase == TouchPhase.Ended)
//                     {
//                         firstTouchTime = touchDeltaTime = 0;
//                     }
//                 }
//             }
//             else
//             {
//                 Move(direction);
//             }
// #endif
        }
    }

    public void Move(int direction = 1) //-1: LR     1: RL   0: Stop
    {
        StartCoroutine(C_Move(direction));
    }

    IEnumerator C_Move(int direction = 1)
    {
        isMoving = true;
        if (direction == 0)
        {
            yield return null;
        }
        else
        {
            index = direction < 0 ? (index - 1) : (index + 1);

            if (index == -1)
            {
                index = wayPoints.Count - 1;
            }
            if (index == wayPoints.Count)
            {
                index = 0;
            }

            FaceToMap();

            Vector3 begin = transform.position;
            Vector3 end = wayPoints[index].position;

            float t = 0f;
            while (t < 1f)
            {
                t += Time.deltaTime / stepDuration;
                transform.position = Vector3.Lerp(begin, end, t);
                yield return null;
            }
        }
        isMoving = false;
    }

    void FaceToMap()
    {
        if (FrontDirection() == Vector3.up)
        {
            transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
        }
        else
        if (FrontDirection() == Vector3.down)
        {
            transform.localRotation = Quaternion.Euler(0f, 0f, 180f);
        }
        else
        if (FrontDirection() == Vector3.left)
        {
            transform.localRotation = Quaternion.Euler(0f, 0f, 90f);
        }
        else
        if (FrontDirection() == Vector3.right)
        {
            transform.localRotation = Quaternion.Euler(0f, 0f, -90f);
        }
    }

    public void ShootBrick()
    {
        if (!isShooting)
        {
            StartCoroutine(C_ShootBrick());
        }
    }

    IEnumerator C_ShootBrick()
    {
        isShooting = true;
        Brick brick = Instantiate(brickPrefab).GetComponent<Brick>();
        brick.transform.SetParent(mapParent);
        brick.transform.position = transform.position;
        brick.transform.localPosition += FrontDirection();
        brick.transform.localRotation = Quaternion.identity;
        brick.direction = FrontDirection();
        brick.isMoved = true;
        yield return new WaitForSeconds(shootingDelay);
        isShooting = false;
    }

    RaycastHit hit;

    bool IsRightFront() //Check RIGHT in front of player
    {
        bool result = false;
        if (Physics.Raycast(transform.position, transform.up, out hit, distToUp + 0.1f, defaultLayer))
        {
            if (hit.transform.CompareTag("Brick") && !hit.transform.gameObject.GetComponent<Brick>().isMoved)
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        else
            return result = false;
        return result;
    }
    bool IsFront() //Check in front of player
    {
        return Physics.Raycast(transform.position, transform.up, 11f, defaultLayer);
    }

    bool IsCornered()
    {
        return index == 6 || index == 18 || index == 30 || index == 42;
    }

    Vector3 FrontDirection()
    {
        if ((index > 42 && index < wayPoints.Count) || (index >= 0 && index < 6))
        {
            return Vector3.up;
        }
        else
        if (index > 6 && index < 18)
        {
            return Vector3.left;
        }
        else
        if (index > 18 && index < 30)
        {
            return Vector3.down;
        }
        else
        if (index > 30 && index < 42)
        {
            return Vector3.right;
        }
        else
        {
            return Vector3.zero;
        }
    }

    #endregion
}
