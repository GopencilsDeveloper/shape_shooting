﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    #region Editor Params
    public GameObject tileEmptyPrefab;
    public ColorToPrefab[] colorMappings;
    #endregion

    #region Params
    Texture2D texture;
    int tileEmptyCount = 0;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    void SetCenter()
    {
        transform.localPosition = new Vector3(-(int)(texture.width / 2) + 1, -(int)(texture.height / 2), 1f);
    }

    void ClearMap()
    {
        tileEmptyCount = 0;
        foreach (Transform item in transform)
        {
            Destroy(item.gameObject);
        }
    }

    public void GenerateMap(int index = 0)
    {
        ClearMap();
        if (index >= MapManager.Instance.mapTextures.Count)
        {
            index = Random.Range(0, MapManager.Instance.mapTextures.Count);
        }
        texture = MapManager.Instance.mapTextures[index];
        SetCenter();
        for (int x = 2; x < 13; x++)    //Ignore Lanes unused --> do NOT Instantiate
        {
            for (int y = 3; y < 14; y++)    //Ignore Lanes unused --> do NOT Instantiate
            {
                GenerateTile(x, y);
            }
        }
    }

    void GenerateTile(int x, int y)
    {
        Color pixelColor = texture.GetPixel(x, y);

        if (pixelColor.a == 0)
        {
            Vector2 position = new Vector2(x, y);
            Instantiate(tileEmptyPrefab, position, Quaternion.identity).transform.SetParent(transform, false);
            tileEmptyCount++;
        }

        MapManager.Instance.currentTileEmptyAmount = tileEmptyCount;

        foreach (ColorToPrefab colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixelColor))
            {
                Vector2 position = new Vector2(x, y);
                Instantiate(colorMapping.prefab, position, Quaternion.identity).transform.SetParent(transform, false);
            }
        }
    }
    #endregion
}
