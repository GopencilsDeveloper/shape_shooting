﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileEmpty : MonoBehaviour
{
    #region Editor Params
    public LayerMask defaultLayer; // NOT TileEmpty
    public MeshRenderer mesh;
    public GameObject popFadeWarning;
    #endregion

    #region Params
    private bool isGameOver = false;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Update()
    {
        if (GameManager.Instance.currentState != GameState.Menu)
        {
            if (IsSurrounded() && !isGameOver)
            {
                Warning();
                GameManager.Instance.GameOver();
                isGameOver = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!other.gameObject.GetComponent<Brick>().isMoved)
        {
            MapManager.Instance.currentTileEmptyAmount--;
            gameObject.SetActive(false);
        }
    }

    // [NaughtyAttributes.Button]
    // public void Test()
    // {
    //     print(IsSurrounded());
    // }

    public bool IsSurrounded()
    {
        bool up = false;
        bool down = false;
        bool left = false;
        bool right = false;

        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.up, out hit, 7f, defaultLayer))
        {
            if (hit.transform.CompareTag("Brick") && !hit.transform.gameObject.GetComponent<Brick>().isMoved)
            {
                up = true;
            }
            if (hit.transform.CompareTag("Tile"))
            {
                up = true;
            }
        }
        if (Physics.Raycast(transform.position, -transform.up, out hit, 7f, defaultLayer))
        {
            if (hit.transform.CompareTag("Brick") && !hit.transform.gameObject.GetComponent<Brick>().isMoved)
            {
                down = true;
            }
            if (hit.transform.CompareTag("Tile"))
            {
                down = true;
            }
        }
        if (Physics.Raycast(transform.position, -transform.right, out hit, 7f, defaultLayer))
        {
            if (hit.transform.CompareTag("Brick") && !hit.transform.gameObject.GetComponent<Brick>().isMoved)
            {
                left = true;
            }
            if (hit.transform.CompareTag("Tile"))
            {
                left = true;
            }
        }
        if (Physics.Raycast(transform.position, transform.right, out hit, 7f, defaultLayer))
        {
            if (hit.transform.CompareTag("Brick") && !hit.transform.gameObject.GetComponent<Brick>().isMoved)
            {
                right = true;
            }
            if (hit.transform.CompareTag("Tile"))
            {
                right = true;
            }
        }

        return up && down && left && right;
    }

    public void Warning()   //Warning when surrounded
    {
        mesh.material.color = Color.red;
        StartCoroutine(C_ShowWarningEffect());
    }


    IEnumerator C_ShowWarningEffect()
    {
        popFadeWarning.SetActive(true);
        yield return new WaitForSeconds(1f);
        popFadeWarning.SetActive(false);
    }

    #endregion
}
