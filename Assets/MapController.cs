﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    [HideInInspector]
    public int order;
    [HideInInspector]
    public int tileEmptyAmount;
    [HideInInspector]
    public int tileAmount;
    [HideInInspector]
    public int tileCoreAmount;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods
    #endregion
}
